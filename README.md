# cocos creator -实现事件监听-On与Emit的实现
### 摘要
CocosCreator 有着内置的事件系统，我们用起来也很是方便。那么我们自己如何简单的实现一个 EventManager 呢？本文通过一个小例子带你学习。

- 使用版本
CocosCreator 版本 2.3.3
- 明确目标
我们要做一个事件管理模块，实现事件的监听方法 on，取消方法 off，事件发送 emit。
![](https://upload-images.jianshu.io/upload_images/8742246-187e1c8debb0e6d1.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

### 事件数据类型
- 首先，我们要想好事件用什么存储。选择用 Map，则需要一个事件名称，类型 string，还有就是一个对象，存放 callback 以及调用者 target。
- 写成单例模式