import { MyEventManager } from "./MyEventManager";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Helloworld extends cc.Component {

    @property(cc.Label)
    label: cc.Label = null;

    onLoad () {

        //注册事件
        MyEventManager.instance().on('event1' , (data)=>{
            console.log("事件1 触发");
            console.log("数据 :" + data);
        }, this);
        //注册
        MyEventManager.instance().on('event2' , (data)=>{
            console.log("事件2 触发");
            console.log("数据 :" + data);
        }, this);
    }
    //按钮btn1
    public onBtnClick1(){
        MyEventManager.instance().emit("event1", {data1:"demo" , data2:[2,2,2]});
    }
    //按钮btn2
    public onBtnClick2(){
        MyEventManager.instance().emit("event2", 1234);
    }
}
